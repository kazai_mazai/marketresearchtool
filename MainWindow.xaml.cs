﻿using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MarketResearchTool.Tools;
using MarketResearchTool.Tools.ToolsHelpers;
using StockSharp.Algo;
using StockSharp.Logging;

namespace MarketResearchTool
{
	using System;
	using System.IO;
	using System.Linq;
	using System.Windows;
	using System.Windows.Forms;
	using System.Windows.Media;
	using System.Globalization;
	using System.Windows.Data;
	using System.Collections.Generic;

	using MessageBox = System.Windows.MessageBox;
	using CheckBox = System.Windows.Controls.CheckBox;
	using ProgressBar = System.Windows.Controls.ProgressBar;

	using Ecng.Xaml;
	using Ecng.Common;
	using Ecng.Collections;
	
	using StockSharp.Algo.Candles;
	using StockSharp.Algo.Storages;
	using StockSharp.Algo.Testing;
	using StockSharp.Algo.Strategies;
	using StockSharp.Algo.Indicators.Trend;
	using StockSharp.BusinessEntities;
	using StockSharp.Xaml;

	public partial class MainWindow
	{
	    private TxtCandleBuilderSource txtCandleBuilderSource;
        private EquityChartWindow _equityChartWindow = new EquityChartWindow();
        private CandlesWindow _candlesWindow = new CandlesWindow();
        private WPF3DChartWindow _chart3dWindow = new WPF3DChartWindow();
        private List<Candle> candles = new List<Candle>();
		public MainWindow()
		{
			InitializeComponent();
		    Start.IsEnabled = true;

            _equityChartWindow.MakeHideable();
            _candlesWindow.MakeHideable();
            _chart3dWindow.MakeHideable();
            initData();
		}

        private void initData()
        {

            var security = new Security();

            security.Code = "SPY";
            security.ExchangeBoard = StockSharp.BusinessEntities.ExchangeBoard.Forts;

            var candleSeries = new CandleSeries(typeof(TimeFrameCandle), security, TimeSpan.FromMinutes(1));

            var candleSource = new TxtCandleBuilderSource(@"J:\MarketData");
            var candleManager = new CandleManager();
            candleManager.Sources.Add(candleSource);

            candleManager.Stopped += (s) => MessageBox.Show(this, "Stopped");
            candleManager.Processing += (s, c) =>
            {
                if (c.State == CandleStates.Finished) candles.Add(c);
            };

            candleManager.Start(candleSeries, new DateTime(2009, 11, 1), new DateTime(2011, 11, 1));

        }


		private void StartBtnClick(object sender, RoutedEventArgs e)
		{
             
             
           
		    var volDist = new IntradayVolumeDistribution();

		  
              
		    for (int i = 0; i < 7; i++)
		    {
                var res1 = volDist.Process(candles.FindAll(c => c.OpenTime.DayOfWeek.To<int>()==i));
                
		        string title = i.To<DayOfWeek>().ToString();


                _equityChartWindow.PlotSequent(this, res1, title);


            }
            if (_equityChartWindow.Visibility != Visibility.Visible)
                _equityChartWindow.Show();
             
         
		    var m = new float[50,50];

		    for (int i = 0; i < 50; i++)
		    {
		        for (int j = 0; j < 50; j++)
		        {
		             
                   
                     double z;
                    double r = 0.15 * Math.Sqrt(2*i*i + 2*j*j);
                    if (r < 1e-10) z = 1;
                    else z = Math.Sin(r) / r;
                    
                     m[i,j]=(float)
		            (z);
		        }
		    }

            
            _chart3dWindow.Plot(m);
            if (_chart3dWindow.Visibility != Visibility.Visible)
                _chart3dWindow.Show();

            _candlesWindow.Plot(candles);
            if (_candlesWindow.Visibility != Visibility.Visible)
                _candlesWindow.Show();
           
            
	  }

        private static void ShowOrHide(Window window)
        {
            if (window == null)
                throw new ArgumentNullException("window");

            if (window.Visibility == Visibility.Visible)
                window.Hide();
            else
                window.Show();
        }

        private void EquityChartWindowButtonClick(object sender, RoutedEventArgs e)
        {
            ShowOrHide(_equityChartWindow);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            
            _equityChartWindow.DeleteHideable();
            _candlesWindow.DeleteHideable();
            _chart3dWindow.DeleteHideable();

            _candlesWindow.Close();
            _equityChartWindow.Close();
            _chart3dWindow.Close();
            
            base.OnClosing(e);
        }

        private void CandleChartButtonClick(object sender, RoutedEventArgs e)
        {
            ShowOrHide(_candlesWindow);
        }

        private void Chart3DButtonClick(object sender, RoutedEventArgs e)
        {
            ShowOrHide(_chart3dWindow);
        }


           




	}



	public class BoolToVisibleConverter : IValueConverter
	{
		public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
		{
			var isChecked = values.To<bool>();
			return isChecked ? Visibility.Visible : Visibility.Hidden;
		}

		public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}