﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StockSharp.Algo.Candles;
using StockSharp.Xaml;

namespace MarketResearchTool
{
    /// <summary>
    /// Логика взаимодействия для CandlesWindow.xaml
    /// </summary>
    public partial class CandlesWindow
    {
        public CandlesWindow()
        {
            InitializeComponent();

            var area = new ChartArea();
            Chart.Areas.Add(area);

            area.Elements.Add(new ChartCandleElement());
        }

        public void ProcessCandles(Candle candle)
        {
            Chart.ProcessCandle((ChartCandleElement)Chart.Areas[0].Elements[0], candle);
        }

        public void  Plot(List<Candle>  candles)
        {
            for (int i = 0; i < candles.Count; i++)
            {
                ProcessCandles(candles[i]);
            }

        }
    }
}
