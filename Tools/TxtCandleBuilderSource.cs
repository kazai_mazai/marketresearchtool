﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Ecng.Common;
using Ecng.ComponentModel;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Candles.Compression;

namespace MarketResearchTool.Tools
{
    public class TxtCandleBuilderSource : ICandleManagerSource 
    {

        protected string _pathToStorageDir = "";
      
        public TxtCandleBuilderSource(string path) 
            
        {
            _pathToStorageDir = path;

             
        }

        public   void Start(CandleSeries series, DateTime from, DateTime to)
        {


            if (series == null) throw new ArgumentNullException("series :" + series.Security.Code);
            if (series.CandleType != typeof (TimeFrameCandle))
                throw new TypedDataSetGeneratorException("Unsupported series type.Symbol: " + series.Security.Code);

            string filePath = "";
            string dirPath = _pathToStorageDir + "\\" + series.Security.Code;
            var dir = new DirectoryInfo(dirPath);
            if (dir.Exists == false) ProcessDataError.SafeInvoke(new DirectoryNotFoundException(series.Security.Code));
            var timeframe = (TimeSpan) series.Arg;
            if (timeframe == new TimeSpan(1, 0, 0, 0))
                filePath = dirPath += "\\" + "OneDay" + ".txt";
            if (timeframe == new TimeSpan(0, 4, 0, 0))
                filePath = dirPath += "\\" + "FourHours" + ".txt";
            if (timeframe == new TimeSpan(0, 1, 0, 0))
                filePath = dirPath += "\\" + "OneHour" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 30, 0))
                filePath = dirPath += "\\" + "ThirtyMinutes" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 15, 0))
                filePath = dirPath += "\\" + "FifteenMinutes" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 5, 0))
                filePath = dirPath += "\\" + "FiveMinutes" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 1, 0))
                filePath = dirPath += "\\" + "OneMinute" + ".txt";

            if (filePath == "")
                ProcessDataError.SafeInvoke(
                    new InvalidDataException("Unsupported timeframe. Symbol: " + series.Security.Code));
            else
            {
                var fileInfo = new FileInfo(filePath);
                if (!fileInfo.Exists)
                    ProcessDataError.SafeInvoke(
                        new FileNotFoundException("Symbol: " + series.Security.Code + ". Timeframe: " +
                                                  timeframe.ToString()));
                else
                {
                    var file = File.OpenText(filePath);

                    var candles = new List<TimeFrameCandle>();
                    while (true)
                    {
                        string line = file.ReadLine();

                        if (line == null) break;
                        else
                        {
                            string[] p = line.Split(',');

                        //    var date = new DateTime(int.Parse(p[0]), int.Parse(p[1]), int.Parse(p[2]), int.Parse(p[3]),int.Parse(p[4]), int.Parse(p[5]));
                        int year =    int.Parse(p[0][0].ToString())*1000 + int.Parse(p[0][1].ToString())*100 +
                            int.Parse(p[0][2].ToString())*10 + int.Parse(p[0][3].ToString());
                            int month = int.Parse(p[0][4].ToString())*10 + int.Parse(p[0][5].ToString());
                            int day = int.Parse(p[0][6].ToString()) * 10 + int.Parse(p[0][7].ToString());


                            int hour = int.Parse(p[1][0].ToString()) * 10 + int.Parse(p[1][1].ToString());
                            int minutes = int.Parse(p[1][2].ToString()) * 10 + int.Parse(p[1][3].ToString());
                            int seconds = int.Parse(p[1][4].ToString()) * 10 + int.Parse(p[1][5].ToString());

                            var date = new DateTime(year, month, day, hour, minutes, seconds);

                            if (date >= from && date <= to)
                            {
                                var candle = new TimeFrameCandle();
                                var culture = CultureInfo.CreateSpecificCulture("en-US");
                                decimal value;
                                if (Decimal.TryParse(p[2],NumberStyles.AllowDecimalPoint,culture, out value)) candle.OpenPrice = value;
                                else
                                    candle.OpenPrice = 0m;
                                if (Decimal.TryParse(p[3], NumberStyles.AllowDecimalPoint, culture, out value)) candle.HighPrice = value;
                                else
                                    candle.HighPrice = 0m;
                                if (Decimal.TryParse(p[4], NumberStyles.AllowDecimalPoint, culture, out value)) candle.LowPrice = value;
                                else
                                    candle.LowPrice = 0m;


                                if (Decimal.TryParse(p[5], NumberStyles.AllowDecimalPoint, culture, out value)) candle.ClosePrice = value;
                                else
                                    candle.ClosePrice = 0m;

                                candle.TimeFrame = timeframe;
                                candle.OpenTime = date;
                                candle.CloseTime = date + timeframe;

                                if (Decimal.TryParse(p[6], NumberStyles.AllowDecimalPoint, culture, out value)) candle.TotalVolume = value;
                                else
                                    candle.TotalVolume = 0m;

                                candle.Security = series.Security;
                                // RaiseProcessing(series, candle);
                                candles.Add(candle);

                            }

                            if (date > to) break;
                        }

                    }
                    file.Close();
                    foreach (var candle in candles)
                    {

                        candle.State = CandleStates.Started;
                        _processing.SafeInvoke(series, candle);
                        candle.State = CandleStates.Finished;
                        _processing.SafeInvoke(series, candle);
                    }
                      
                }

            }
        }

        public   IEnumerable<Range<DateTime>> GetSupportedRanges(CandleSeries series)
        {
            if (series == null) throw new ArgumentNullException("series :" + series.Security.Code);
            if (series.CandleType != typeof(TimeFrameCandle)) throw new TypedDataSetGeneratorException("series type: " + series.Security.Code);

            string filePath = "";
            string dirPath = _pathToStorageDir + "\\" + series.Security.Code;
            var dir = new DirectoryInfo(dirPath);
            if (dir.Exists == false) ProcessDataError.SafeInvoke(new DirectoryNotFoundException(series.Security.Code));
            var timeframe = (TimeSpan)series.Arg;
            if (timeframe == new TimeSpan(1, 0, 0, 0))
                filePath = dirPath += "\\" + "OneDay" + ".txt";
            if (timeframe == new TimeSpan(0, 4, 0, 0))
                filePath = dirPath += "\\" + "FourHours" + ".txt";
            if (timeframe == new TimeSpan(0, 1, 0, 0))
                filePath = dirPath += "\\" + "OneHour" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 30, 0))
                filePath = dirPath += "\\" + "ThirtyMinutes" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 15, 0))
                filePath = dirPath += "\\" + "FifteenMinutes" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 5, 0))
                filePath = dirPath += "\\" + "FiveMinutes" + ".txt";
            if (timeframe == new TimeSpan(0, 0, 1, 0))
                filePath = dirPath += "\\" + "OneMinute" + ".txt";

            if (filePath == "") ProcessDataError.SafeInvoke(new InvalidDataException("unsupported timeframe " + series.Security.Code));
            else
            {
                var fileInfo = new FileInfo(filePath);
                if (!fileInfo.Exists)
                    ProcessDataError.SafeInvoke(
                        new FileNotFoundException("symbol: " + series.Security.Code + " timeframe: " +
                                                  timeframe.ToString()));
                else
                {
                    var file = File.OpenText(filePath);
                    string line = file.ReadLine();
                    if (line != null)
                    {
                        string[] p = line.Split(',');
                        int year = int.Parse(p[0][0].ToString()) * 1000 + int.Parse(p[0][1].ToString()) * 100 +
                            int.Parse(p[0][2].ToString()) * 10 + int.Parse(p[0][3].ToString());
                        int month = int.Parse(p[0][4].ToString()) * 10 + int.Parse(p[0][5].ToString());
                        int day = int.Parse(p[0][6].ToString()) * 10 + int.Parse(p[0][7].ToString());


                        int hour = int.Parse(p[1][0].ToString()) * 10 + int.Parse(p[1][1].ToString());
                        int minutes = int.Parse(p[1][2].ToString()) * 10 + int.Parse(p[1][3].ToString());
                        int seconds = int.Parse(p[1][4].ToString()) * 10 + int.Parse(p[1][5].ToString());
                         
                        var startDate = new DateTime(year, month, day, hour, minutes, seconds);
                        DateTime endDate = startDate;
                        while (true)
                        {
                            line = file.ReadLine();

                            if (line == null) break;
                            else
                            {
                                 p = line.Split(',');
                                   year = int.Parse(p[0][0].ToString()) * 1000 + int.Parse(p[0][1].ToString()) * 100 +
                             int.Parse(p[0][2].ToString()) * 10 + int.Parse(p[0][3].ToString());
                                   month = int.Parse(p[0][4].ToString()) * 10 + int.Parse(p[0][5].ToString());
                                   day = int.Parse(p[0][6].ToString()) * 10 + int.Parse(p[0][7].ToString());


                                   hour = int.Parse(p[1][0].ToString()) * 10 + int.Parse(p[1][1].ToString());
                                   minutes = int.Parse(p[1][2].ToString()) * 10 + int.Parse(p[1][3].ToString());
                                   seconds = int.Parse(p[1][4].ToString()) * 10 + int.Parse(p[1][5].ToString());


                                   endDate = new DateTime(year, month, day, hour, minutes, seconds);



                            }


                        }
                        var timeRanges = new List<Range<DateTime>>();
                        var range = new Range<DateTime>(startDate, endDate);
                        timeRanges.Add(range);
                        file.Close();
                        return timeRanges;


                    }
                    else
                    {
                        file.Close();
                        return new List<Range<DateTime>>();
                    }











                }
            }

            return new List<Range<DateTime>>();
        }


        private Action<CandleSeries, Candle> _processing;


        public void Stop(CandleSeries series)
        {

        }

        public int SpeedPriority
        {
            get { return 0; }
        }

        event Action<CandleSeries, Candle> ICandleSource<Candle>.Processing
        {
            add { _processing += value; }
            remove { _processing -= value; }
        }

        public event Action<CandleSeries> Stopped;
        public event Action<Exception> ProcessDataError;

        public event Action<CandleSeries, IEnumerable<ICandleBuilderSourceValue>> Processing;
        public ICandleManager CandleManager { get; set; }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
