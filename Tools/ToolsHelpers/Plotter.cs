﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Threading;
using Ecng.Xaml;
using StockSharp.Xaml;

namespace MarketResearchTool.Tools.ToolsHelpers
{
   public class Plotter
    {

       private DateTime _lastSequenceEndDate= new DateTime(2010,1,1,0,0,0);
       private DateTime _lastSequenceStartDate = new DateTime(2010, 1, 1, 0, 0, 0);

    public    void PlotParallel(DispatcherObject dispatcherObject, EquityCurveChart chart, List<ObjectPoint> pointList, string title )
       {

            if(pointList.Count==0) return;

         
           var r = new Random();
           var rand = new byte[3];
           r.NextBytes(rand);
            var color = Color.FromRgb(rand[0], rand[1], rand[2]);

            var curve = chart.CreateCurve(title, color);


            DateTime pointListPreviousDate = (DateTime)pointList[0].X;
            DateTime sequencePreviousDate = new DateTime(_lastSequenceStartDate.Ticks);


           for (int i = 0; i < pointList.Count; i++)
           {
               var timeSpan = (DateTime)pointList[i].X - pointListPreviousDate;
               var dateTimeToWrite = sequencePreviousDate + timeSpan;

               var data = new EquityData
               {
                   Time = dateTimeToWrite,
                   Value =(decimal) pointList[i].Y
               };

               dispatcherObject.GuiAsync(() => curve.Add(data));

               sequencePreviousDate = dateTimeToWrite;
               pointListPreviousDate = (DateTime) pointList[i].X;
               if (dateTimeToWrite > _lastSequenceEndDate) _lastSequenceEndDate = dateTimeToWrite;

           }

       }

    public    void PlotSequent(DispatcherObject dispatcherObject, EquityCurveChart chart, List<ObjectPoint> pointList, string title)
        {

            if (pointList.Count == 0) return;


            var r = new Random();
            var rand = new byte[3];
            r.NextBytes(rand);
            var color = Color.FromRgb(rand[0], rand[1], rand[2]);

            var curve = chart.CreateCurve(title, color);


            DateTime pointListPreviousDate = (DateTime)pointList[0].X;

            _lastSequenceEndDate = new DateTime(_lastSequenceEndDate.Year, _lastSequenceEndDate.Month, _lastSequenceEndDate.Day+1,0,0,0);

            for (int i = 0; i < pointList.Count; i++)
            {
                var timeSpan = (DateTime)pointList[i].X - pointListPreviousDate;
                var dateTimeToWrite = _lastSequenceEndDate + timeSpan;

                var data = new EquityData
                {
                    Time = dateTimeToWrite,
                    Value = (decimal)pointList[i].Y
                };

                dispatcherObject.GuiAsync(() => curve.Add(data));

                _lastSequenceEndDate = dateTimeToWrite;
                pointListPreviousDate = (DateTime)pointList[i].X;
                

            }

        }

       
     public  void PlotTrueToDate(DispatcherObject dispatcherObject, EquityCurveChart chart, List<ObjectPoint> pointList, string title)
     {
         {

             if (pointList.Count == 0) return;


             var r = new Random();
             var rand = new byte[3];
             r.NextBytes(rand);
             var color = Color.FromRgb(rand[0], rand[1], rand[2]);

             var curve = chart.CreateCurve(title, color);


             
           
             for (int i = 0; i < pointList.Count; i++)
             {
             
                 var data = new EquityData
                 {
                     Time = (DateTime)pointList[i].X,
                     Value = (decimal)pointList[i].Y
                 };

                 dispatcherObject.GuiAsync(() => curve.Add(data));
                  

             }

         }


     }
   
   
   
   }
}
