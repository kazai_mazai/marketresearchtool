﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarketResearchTool.Tools.ToolsHelpers
{
    public class ObjectPoint
    {
        private Object _x;
        private Object _y;
        

        public object X
        {
            get { return _x; }
            set { _x = value;
                 
            }
        }

        public object Y
        {
            get { return _y; }
            set { _y = value;
               
            }
        }

       
    }
}
