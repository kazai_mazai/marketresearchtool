﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarketResearchTool.Tools.ToolsHelpers;
using StockSharp.Algo.Candles;

namespace MarketResearchTool.Tools
{
  public  class IntradayVolumeDistribution
  {
      private decimal[] _valuesArray;
      private int[] _countArray;




      public List<ObjectPoint> Process(List<Candle> candles)
    {

          int minutesPerDay = 24 * 60;

          List<ObjectPoint> resultArray = new List<ObjectPoint>(minutesPerDay);
          decimal[] ValuesArray = new decimal[minutesPerDay];
          int[] CountArray = new int[minutesPerDay];

           if(candles==null) throw new ArgumentNullException("Candles");
          
           if (candles.Count > 0)
           {
               if ((TimeSpan) candles[0].Arg != TimeSpan.FromMinutes(1)) throw new ArgumentException("wrong timeframe");

               for (int i = 0; i < candles.Count; i++)
               {
                   DateTime dayStart = new DateTime(candles[i].OpenTime.Year, candles[i].OpenTime.Month,
                                                    candles[i].OpenTime.Day, 0, 0, 0);
                   int candleNumber = (candles[i].OpenTime - dayStart).Hours*60 +
                                      (candles[i].OpenTime - dayStart).Minutes;

                   ValuesArray[candleNumber] += candles[i].TotalVolume;
                   CountArray[candleNumber]++;
               }




               var startDate = new DateTime(2012, 5, 13, 0, 0, 0);

               for (int i = 1; i < ValuesArray.Length; i++)
               {
                   if (CountArray[i] > 0)
                       ValuesArray[i] = ValuesArray[i]/CountArray[i];

                   var point = new ObjectPoint();
                     point.Y = ValuesArray[i];
                   point.X = startDate + TimeSpan.FromMinutes(i);

                   resultArray.Add(point);


               }
           }
          return resultArray;
            



        



    }


    }
}
